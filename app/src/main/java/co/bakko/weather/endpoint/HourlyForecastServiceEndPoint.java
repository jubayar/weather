package co.bakko.weather.endpoint;

import co.bakko.weather.model.ForecastResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HourlyForecastServiceEndPoint {

    @GET("forecast")
    Call<ForecastResponse> getHourlyForecastByCityId (@Query("id") String cityId, @Query("APPID") String appKey);

    @GET("forecast")
    Call<ForecastResponse> getHourlyForecastByCoordinate (@Query("lat") double latitude, @Query("lon") double longitude, @Query("APPID") String appKey);
}
