package co.bakko.weather.endpoint;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.facebook.FacebookSdk.getCacheDir;


public class ServiceGenerator {

    private static final String API_BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private static int cacheSize = 10 * 1024 * 1024; // 10 MB
    private static Cache cache = new Cache(getCacheDir(), cacheSize);

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .cache(cache)
            .build();

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}