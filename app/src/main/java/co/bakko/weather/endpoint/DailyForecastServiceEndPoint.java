package co.bakko.weather.endpoint;

import co.bakko.weather.model.DailyForecastResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DailyForecastServiceEndPoint {

    @GET("forecast/daily")
    Call<DailyForecastResponse> getDailyForecastByCoordinate (@Query("lat") double latitude, @Query("lon") double longitude, @Query("cnt") int days, @Query("APPID") String appKey);
}
