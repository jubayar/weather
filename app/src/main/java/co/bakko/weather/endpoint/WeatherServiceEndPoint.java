package co.bakko.weather.endpoint;

import co.bakko.weather.model.WeatherResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherServiceEndPoint {

    @GET("weather")
    Call<WeatherResponse> getCurrentWeatherByCityId(@Query("id") String cityId, @Query("APPID") String appKey);

    @GET("weather")
    Call<WeatherResponse> getCurrentWeatherByCoordinate(@Query("lat") double latitude, @Query("lon") double longitude, @Query("APPID") String appKey);
}