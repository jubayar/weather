package co.bakko.weather.model;

import com.google.gson.annotations.SerializedName;

public class City {
    public int id;
    public String name;
    @SerializedName("coord")
    public Coordinate coordinate;
    public String country;
}
