package co.bakko.weather.model;

public class SysInfo {
    public int type;
    public int id;
    public double message;
    public String country;
    public int sunrise;
    public int sunset;
}
