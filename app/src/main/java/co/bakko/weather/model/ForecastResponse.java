package co.bakko.weather.model;

import java.util.List;

public class ForecastResponse {
    public String cod;
    public double message;
    public int cnt;
    public List<ForecastWeather> list = null;
    public City city;
}

