package co.bakko.weather.model;

public class TempInfo {
    public double day;
    public double min;
    public double max;
    public double night;
    public double eve;
    public double morn;
}
