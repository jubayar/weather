package co.bakko.weather.model;

public class ForecastWeather {
    public int dt;
    public MainWeatherInfo main;
    public java.util.List<Weather> weather = null;
    public Clouds clouds;
    public Wind wind;
    public Snow snow;
    public Sys sys;
    public String dt_txt;
}

class Sys {
    public String pod;
}
