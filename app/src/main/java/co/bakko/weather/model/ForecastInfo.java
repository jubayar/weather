package co.bakko.weather.model;

public class ForecastInfo {
    public String forecastTime;
    public String temperature;
    public String description;
    public String windInfo;
    public String humidity;

    public String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
