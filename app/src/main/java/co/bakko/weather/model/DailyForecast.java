package co.bakko.weather.model;

import java.util.List;

public class DailyForecast {
    public int dt;
    public TempInfo temp;
    public double pressure;
    public int humidity;
    public List<Weather> weather;
    public double speed;
    public double deg;
    public double clouds;
}
