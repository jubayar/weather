package co.bakko.weather.model;

public class Weather {
    public int id;
    public String main;
    public String description;
    public String icon;
}
