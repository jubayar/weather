package co.bakko.weather.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherResponse {
    public Coordinate coord;
    public List<Weather> weather = null;
    public String base;
    public MainWeatherInfo main;
    public int visibility;
    public Wind wind;
    public Clouds clouds;
    public int dt;
    public SysInfo sys;
    @SerializedName("id")
    public int cityId;
    @SerializedName("name")
    public String cityName;
    public int cod;
}