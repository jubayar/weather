package co.bakko.weather.model;

import java.util.List;

public class DailyForecastResponse {
    public int cod;
    public String message;
    public City city;
    public int cnt;
    public List<DailyForecast> list;
}
