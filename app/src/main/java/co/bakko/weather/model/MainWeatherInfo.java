package co.bakko.weather.model;

public class MainWeatherInfo {

    public double temp;
    public double pressure;
    public int humidity;

    public double temp_min;
    public double temp_max;

    public double sea_level;
    public double grnd_level;

    public double temp_kf;
}
