package co.bakko.weather.model;

public class CurrentWeather {
    public String lastUpdate;
    public String temperature;
    public String description;
    public String windInfo;
    public String pressure;
    public String humidity;
    public String sunRise;
    public String sunSet;
    public String coordinate;
    public String cityName;
}
