package co.bakko.weather.model;

public class DailyInfo {
    public String forecastTime;
    public String temp;
    public String minTemp;
    public String maxTemp;
    public String description;
    public String humidity;
    public String clouds;
    public String windInfo;
}
