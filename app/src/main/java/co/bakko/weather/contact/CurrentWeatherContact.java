package co.bakko.weather.contact;

import android.content.Context;

import co.bakko.weather.model.CurrentWeather;
import co.bakko.weather.model.Weather;
import co.bakko.weather.model.WeatherResponse;

public interface CurrentWeatherContact {

    interface UserView {

        void setSwipeRefreshLayout(boolean enable);

        void showNoInternetLayout(boolean noInternet);

        void showErrorMsg(String msg);

        void showWeatherInfo(CurrentWeather weather);
    }

    interface UserActionListener {

        void onStart(Context context);

        void checkSetting(Context context);

        void fetchWeatherInfo();

        void onDestroy();
    }
}
