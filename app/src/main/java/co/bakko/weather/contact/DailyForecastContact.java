package co.bakko.weather.contact;

import android.content.Context;

import java.util.List;

import co.bakko.weather.model.DailyInfo;

public interface DailyForecastContact {

    interface UserView {

        void setSwipeRefreshLayout(boolean enable);

        void showNoInternetLayout(boolean noInternet);

        void showErrorMsg(String msg);

        void showWeatherInfo(List<DailyInfo> dailyInfos);
    }

    interface UserActionListener {

        void onStart(Context context);

        void checkSetting(Context context);

        void fetchWeatherForecast();

        void onDestroy();
    }
}
