package co.bakko.weather.contact;

import android.content.Context;

import java.util.List;

import co.bakko.weather.model.CurrentWeather;
import co.bakko.weather.model.ForecastInfo;
import co.bakko.weather.model.ForecastWeather;

public interface HourlyForecastContact {

    interface UserView {

        void setSwipeRefreshLayout(boolean enable);

        void showNoInternetLayout(boolean noInternet);

        void showErrorMsg(String msg);

        void showWeatherInfo(List<ForecastInfo> forecastList);
    }

    interface UserActionListener {

        void onStart(Context context);

        void checkSetting(Context context);

        void fetchWeatherForecast();

        void onDestroy();
    }
}

