package co.bakko.weather.contact.callback;

public interface ResultCallback<T> {
    void isErrors(boolean isError, String msg);
    void onSuccess(T result);
}