package co.bakko.weather.contact;

import android.location.Location;

import com.facebook.CallbackManager;

import co.bakko.weather.view.LoginScreenActivity;
import co.bakko.weather.view.LoginScreenFragment;

public interface LoginScreenContact {

    interface UserView {

        void showProgressIndicator(boolean showProgress);

        void showNoInternetLayout(boolean noInternet);

        void showMsg(String msg);

        void enableButton(boolean enable);
    }

    interface UserActionListener {

        void connectLocationService();

        void facebookLogin(LoginScreenFragment fragment, CallbackManager callbackManager);

        void forwardToMainView(Location location);

        void onDestroy();
    }
}
