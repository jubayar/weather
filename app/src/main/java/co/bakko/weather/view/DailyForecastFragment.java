package co.bakko.weather.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.bakko.weather.R;
import co.bakko.weather.contact.DailyForecastContact.UserActionListener;
import co.bakko.weather.contact.DailyForecastContact.UserView;
import co.bakko.weather.databinding.FragmentDailyForecastBinding;
import co.bakko.weather.model.DailyInfo;
import co.bakko.weather.presenter.DailyForecastPresenter;

public class DailyForecastFragment extends Fragment implements UserView, SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_LATITUDE = "latitude";
    private static final String ARG_LONGITUDE = "longitude";

    private double latitude;
    private double longitude;
    private Context context;

    private DailyForecastAdapter adapter;
    private List<DailyInfo> forecasts;

    private FragmentDailyForecastBinding bindingView;
    private UserActionListener userActionListener;

    public DailyForecastFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            latitude = getArguments().getDouble(ARG_LATITUDE);
            longitude = getArguments().getDouble(ARG_LONGITUDE);
        }
        userActionListener = new DailyForecastPresenter(this, latitude, longitude);
        forecasts = new ArrayList<DailyInfo>();
        adapter = new DailyForecastAdapter(forecasts);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bindingView = DataBindingUtil.inflate(inflater, R.layout.fragment_daily_forecast, container, false);

        bindingView.recycleViewForecast.setHasFixedSize(true);
        bindingView.recycleViewForecast.setLayoutManager(new LinearLayoutManager(context));
        bindingView.recycleViewForecast.setAdapter(adapter);
        bindingView.swipeRefreshLayout.setOnRefreshListener(this);

        return bindingView.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        userActionListener.onStart(getContext());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
        userActionListener.onDestroy();
    }

    @Override
    public void setSwipeRefreshLayout(boolean enable) {
        bindingView.swipeRefreshLayout.setRefreshing(enable);
        bindingView.recycleViewForecast.setVisibility(View.GONE);
    }

    @Override
    public void showNoInternetLayout(boolean noInternet) {
        if (noInternet) {
            Toast.makeText(context, "No internet, Check your internet Setting.", Toast.LENGTH_SHORT).show();
            bindingView.recycleViewForecast.setVisibility(View.GONE);
        }
    }

    @Override
    public void showErrorMsg(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showWeatherInfo(List<DailyInfo> dailyInfos) {
        bindingView.recycleViewForecast.setVisibility(View.VISIBLE);
        forecasts.addAll(dailyInfos);
        adapter.notifyDataChanged();
    }

    @Override
    public void onRefresh() {
        userActionListener.checkSetting(getContext());
    }

    public static DailyForecastFragment newInstance(double latitude, double longitude) {
        DailyForecastFragment fragment = new DailyForecastFragment();
        Bundle args = new Bundle();
        args.putDouble(ARG_LATITUDE, latitude);
        args.putDouble(ARG_LONGITUDE, longitude);
        fragment.setArguments(args);
        return fragment;
    }
}
