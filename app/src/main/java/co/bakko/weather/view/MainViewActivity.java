package co.bakko.weather.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.facebook.login.LoginManager;

import co.bakko.weather.R;

public class MainViewActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Toolbar.OnMenuItemClickListener {

    private static final String ARG_LATITUDE = "latitude";
    private static final String ARG_LONGITUDE = "longitude";

    private NavigationView navigationView;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private Toolbar toolbar;

    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_view);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main_view);
        toolbar.setOnMenuItemClickListener(this);

        if (getIntent().getExtras() != null) {
            latitude = getIntent().getDoubleExtra(ARG_LATITUDE, 0.0);
            longitude = getIntent().getDoubleExtra(ARG_LONGITUDE, 0.0);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setHomeAsNavigationView();
    }

    private void setHomeAsNavigationView() {
        navigationView.setCheckedItem(R.id.nav_weather);
        navigationView.getMenu().performIdentifierAction(R.id.nav_weather, 0);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            LoginManager.getInstance().logOut();
            LoginScreenActivity.start(this);
            finish();
            return true;
        }
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_weather) {
            fragment = CurrentWeatherFragment.newInstance(latitude, longitude);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, fragment);
            transaction.commit();
            toolbar.setTitle("Weather");
        } else if (id == R.id.nav_forecast) {
            fragment = HourlyForecastFragment.newInstance(latitude, longitude);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, fragment);
            transaction.commit();
            toolbar.setTitle("Hourly Forecast");
        } else if (id == R.id.nav_daily_forecast) {
            fragment = DailyForecastFragment.newInstance(latitude, longitude);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, fragment);
            transaction.commit();
            toolbar.setTitle("Daily Forecast");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void start(Context context, double latitude, double longitude) {
        Intent starter = new Intent(context, MainViewActivity.class);
        starter.putExtra(ARG_LATITUDE, latitude);
        starter.putExtra(ARG_LONGITUDE, longitude);
        context.startActivity(starter);
        ((Activity)context).finish();
    }
}
