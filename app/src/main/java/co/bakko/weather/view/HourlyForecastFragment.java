package co.bakko.weather.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.bakko.weather.R;
import co.bakko.weather.contact.HourlyForecastContact;
import co.bakko.weather.contact.HourlyForecastContact.UserView;
import co.bakko.weather.databinding.FragmentHourlyForecastBinding;
import co.bakko.weather.model.ForecastInfo;
import co.bakko.weather.presenter.HourlyForecastPresenter;

public class HourlyForecastFragment extends Fragment implements UserView, SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_LATITUDE = "latitude";
    private static final String ARG_LONGITUDE = "longitude";

    private double latitude;
    private double longitude;
    private Context context;

    private HourlyForecastAdapter adapter;
    private List<ForecastInfo> weatherList;

    private FragmentHourlyForecastBinding forecastBinding;
    private HourlyForecastContact.UserActionListener userActionListener;

    public HourlyForecastFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            latitude = getArguments().getDouble(ARG_LATITUDE);
            longitude = getArguments().getDouble(ARG_LONGITUDE);
        }

        userActionListener = new HourlyForecastPresenter(this, latitude, longitude);
        weatherList = new ArrayList<ForecastInfo>();
        adapter = new HourlyForecastAdapter(weatherList);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        forecastBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_hourly_forecast, container, false);

        forecastBinding.recycleViewWeather.setHasFixedSize(true);
        forecastBinding.recycleViewWeather.setLayoutManager(new LinearLayoutManager(context));
        forecastBinding.recycleViewWeather.setAdapter(adapter);

        forecastBinding.swipeRefreshLayout.setOnRefreshListener(this);

        ForecastInfo info = new ForecastInfo();
        info.setType("load");
        weatherList.add(info);
        adapter.notifyItemInserted(weatherList.size()-1);

        return forecastBinding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        userActionListener.onStart(getContext());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
        userActionListener.onDestroy();
    }

    @Override
    public void setSwipeRefreshLayout(boolean enable) {
        forecastBinding.swipeRefreshLayout.setRefreshing(enable);
        forecastBinding.recycleViewWeather.setVisibility(View.GONE);
    }

    @Override
    public void showNoInternetLayout(boolean noInternet) {
        if (noInternet) {
            Toast.makeText(context, "No internet, Check your internet Setting.", Toast.LENGTH_SHORT).show();
            forecastBinding.recycleViewWeather.setVisibility(View.GONE);
        }
    }

    @Override
    public void showErrorMsg(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showWeatherInfo(List<ForecastInfo> forecastWeatherList) {
        forecastBinding.recycleViewWeather.setVisibility(View.VISIBLE);
        weatherList.addAll(forecastWeatherList);
        weatherList.remove(0);
        adapter.notifyDataChanged();
    }

    @Override
    public void onRefresh() {
        userActionListener.checkSetting(getContext());
    }

    public static HourlyForecastFragment newInstance(double latitude, double longitude) {
        HourlyForecastFragment fragment = new HourlyForecastFragment();
        Bundle args = new Bundle();
        args.putDouble(ARG_LATITUDE, latitude);
        args.putDouble(ARG_LONGITUDE, longitude);
        fragment.setArguments(args);
        return fragment;
    }
}
