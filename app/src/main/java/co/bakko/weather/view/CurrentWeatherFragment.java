package co.bakko.weather.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import co.bakko.weather.R;
import co.bakko.weather.contact.CurrentWeatherContact;
import co.bakko.weather.databinding.FragmentCurrentWeatherBinding;
import co.bakko.weather.model.CurrentWeather;
import co.bakko.weather.presenter.CurrentWeatherPresenter;

public class CurrentWeatherFragment extends Fragment implements CurrentWeatherContact.UserView, SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_LATITUDE = "latitude";
    private static final String ARG_LONGITUDE = "longitude";

    private double latitude;
    private double longitude;
    private Context context;

    private FragmentCurrentWeatherBinding weatherBinding;
    private CurrentWeatherContact.UserActionListener userActionListener;

    public CurrentWeatherFragment() {}

    public static CurrentWeatherFragment newInstance(double latitude, double longitude) {
        CurrentWeatherFragment fragment = new CurrentWeatherFragment();
        Bundle args = new Bundle();
        args.putDouble(ARG_LATITUDE, latitude);
        args.putDouble(ARG_LONGITUDE, longitude);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            latitude = getArguments().getDouble(ARG_LATITUDE);
            longitude = getArguments().getDouble(ARG_LONGITUDE);
        }
        userActionListener = new CurrentWeatherPresenter(this, latitude, longitude);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        weatherBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_current_weather, container, false);
        weatherBinding.swipeRefreshLayout.setOnRefreshListener(this);
        return weatherBinding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        userActionListener.onStart(getContext());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
        userActionListener.onDestroy();
    }

    @Override
    public void setSwipeRefreshLayout(boolean enable) {
        weatherBinding.swipeRefreshLayout.setRefreshing(enable);
        weatherBinding.layoutData.setVisibility(View.GONE);
    }

    @Override
    public void showNoInternetLayout(boolean noInternet) {
        if (noInternet) {
            Toast.makeText(context, "No internet, Check your internet Setting.", Toast.LENGTH_SHORT).show();
            weatherBinding.layoutData.setVisibility(View.GONE);
        }
    }

    @Override
    public void showErrorMsg(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showWeatherInfo(CurrentWeather weather) {
        weatherBinding.layoutData.setVisibility(View.VISIBLE);
        weatherBinding.setWeather(weather);
    }

    @Override
    public void onRefresh() {
        userActionListener.checkSetting(getContext());
    }
}
