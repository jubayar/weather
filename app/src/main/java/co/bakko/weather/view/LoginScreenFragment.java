package co.bakko.weather.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.CallbackManager;

import co.bakko.weather.R;
import co.bakko.weather.contact.LoginScreenContact;
import co.bakko.weather.contact.LoginScreenContact.UserView;
import co.bakko.weather.databinding.FragmentLoginScreenBinding;
import co.bakko.weather.presenter.LoginScreenPresenter;

public class LoginScreenFragment extends Fragment implements UserView {

    private LoginScreenContact.UserActionListener userActionListener;
    private CallbackManager callbackManager;

    private FragmentLoginScreenBinding bindingView;
    private ProgressBar progressBar;

    public LoginScreenFragment() { }

    public static LoginScreenFragment newInstance() {
        LoginScreenFragment fragment = new LoginScreenFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userActionListener = new LoginScreenPresenter(getContext(), this);
        callbackManager = CallbackManager.Factory.create();
        userActionListener.connectLocationService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bindingView = DataBindingUtil.inflate(inflater, R.layout.fragment_login_screen, container, false);
        bindingView.buttonLobin .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userActionListener.facebookLogin(LoginScreenFragment.this, callbackManager);
            }
        });
        progressBar = new ProgressBar(getContext(), null, android.R.attr.progressBarStyleSmall);
        progressBar.setVisibility(View.VISIBLE);
        return bindingView.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        userActionListener.onDestroy();
    }

    @Override
    public void showProgressIndicator(boolean showProgress) {
        if (showProgress) {
            bindingView.progressBar.setVisibility(View.VISIBLE);
        } else {
            bindingView.progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNoInternetLayout(boolean noInternet) {
        if (noInternet)
            Toast.makeText(getContext(), "Check your internet connection", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMsg(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void enableButton(boolean enable) {
        bindingView.buttonLobin.setEnabled(enable);
    }
}
