package co.bakko.weather.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import co.bakko.weather.databinding.RowHourlyForecastBinding;
import co.bakko.weather.model.ForecastInfo;

public class HourlyForecastAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ForecastInfo> weatherList;

    public HourlyForecastAdapter(List<ForecastInfo> weatherList) {
        this.weatherList = weatherList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowHourlyForecastBinding forecastBinding = RowHourlyForecastBinding.inflate(layoutInflater, parent, false);
        return new ForecastItemHolder(forecastBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ForecastItemHolder)holder).bindData(weatherList.get(position));
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    public class ForecastItemHolder extends RecyclerView.ViewHolder{
        private final RowHourlyForecastBinding forecastBinding;

        public ForecastItemHolder(RowHourlyForecastBinding binding) {
            super(binding.getRoot());
            this.forecastBinding = binding;
        }

        void bindData(final ForecastInfo forecastInfo) {
            forecastBinding.setWeather(forecastInfo);
            forecastBinding.executePendingBindings();
        }
    }

    public void notifyDataChanged(){
        notifyDataSetChanged();
    }
}
