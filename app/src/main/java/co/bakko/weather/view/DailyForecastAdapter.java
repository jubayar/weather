package co.bakko.weather.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import co.bakko.weather.databinding.RowDailyForecastBinding;
import co.bakko.weather.model.DailyInfo;

public class DailyForecastAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<DailyInfo> dailyInfos;

    public DailyForecastAdapter(List<DailyInfo> dailyInfos) {
        this.dailyInfos = dailyInfos;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowDailyForecastBinding binding = RowDailyForecastBinding.inflate(layoutInflater, parent, false);
        return new DailyForecastItemHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((DailyForecastItemHolder)holder).bindData(dailyInfos.get(position));
    }

    @Override
    public int getItemCount() {
        return dailyInfos.size();
    }

    public class DailyForecastItemHolder extends RecyclerView.ViewHolder{

        private final RowDailyForecastBinding forecastBinding;

        public DailyForecastItemHolder(RowDailyForecastBinding binding) {
            super(binding.getRoot());
            this.forecastBinding = binding;
        }

        void bindData(final DailyInfo dailyInfo) {
            forecastBinding.setWeather(dailyInfo);
            forecastBinding.executePendingBindings();
        }
    }

    public void notifyDataChanged(){
        notifyDataSetChanged();
    }
}
