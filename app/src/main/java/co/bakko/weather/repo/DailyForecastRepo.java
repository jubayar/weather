package co.bakko.weather.repo;

import co.bakko.weather.contact.callback.ResultCallback;
import co.bakko.weather.endpoint.DailyForecastServiceEndPoint;
import co.bakko.weather.endpoint.ServiceGenerator;
import co.bakko.weather.model.DailyForecastResponse;
import co.bakko.weather.util.AppUtils;
import co.bakko.weather.util.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailyForecastRepo {

    private DailyForecastServiceEndPoint endPoint = ServiceGenerator.createService(DailyForecastServiceEndPoint.class);

    public void getDailyForecastByCoordinate(double latitude, double longitude, int days, final ResultCallback<DailyForecastResponse> callback) {
        Call<DailyForecastResponse> call = endPoint.getDailyForecastByCoordinate(latitude, longitude, days, AppUtils.apiKey);

        call.enqueue(new Callback<DailyForecastResponse>() {
            @Override
            public void onResponse(Call<DailyForecastResponse> call, Response<DailyForecastResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    callback.isErrors(true, NetworkUtils.errorString(response.code()));
                }
            }

            @Override
            public void onFailure(Call<DailyForecastResponse> call, Throwable t) {
                callback.isErrors(true, "Network failure, retry.");
            }
        });
    }
}
