package co.bakko.weather.repo;

import co.bakko.weather.contact.callback.ResultCallback;
import co.bakko.weather.endpoint.HourlyForecastServiceEndPoint;
import co.bakko.weather.endpoint.ServiceGenerator;
import co.bakko.weather.model.ForecastResponse;
import co.bakko.weather.util.AppUtils;
import co.bakko.weather.util.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HourlyForecastRepo {

    private HourlyForecastServiceEndPoint endPoint = ServiceGenerator.createService(HourlyForecastServiceEndPoint.class);

    public void get5DaysForecastByCityId(String cityId, final ResultCallback<ForecastResponse> callback) {
        Call<ForecastResponse> call = endPoint.getHourlyForecastByCityId(cityId, AppUtils.apiKey);

        call.enqueue(new Callback<ForecastResponse>() {
            @Override
            public void onResponse(Call<ForecastResponse> call, Response<ForecastResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    callback.isErrors(true, String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ForecastResponse> call, Throwable t) {
                callback.isErrors(true, t.getMessage());
            }
        });
    }

    public void get5DaysForecastByCoordinate(double latitude, double longitude, final ResultCallback<ForecastResponse> callback) {

        Call<ForecastResponse> call = endPoint.getHourlyForecastByCoordinate(latitude, longitude, AppUtils.apiKey);

        call.enqueue(new Callback<ForecastResponse>() {
            @Override
            public void onResponse(Call<ForecastResponse> call, Response<ForecastResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    callback.isErrors(true, NetworkUtils.errorString(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ForecastResponse> call, Throwable t) {
                callback.isErrors(true, "Network failure, retry.");
            }
        });
    }

}
