package co.bakko.weather.repo;

import com.crashlytics.android.Crashlytics;

import java.text.SimpleDateFormat;
import java.util.Date;

import co.bakko.weather.contact.callback.ResultCallback;
import co.bakko.weather.endpoint.ServiceGenerator;
import co.bakko.weather.endpoint.WeatherServiceEndPoint;
import co.bakko.weather.model.CurrentWeather;
import co.bakko.weather.model.WeatherResponse;
import co.bakko.weather.util.AppUtils;
import co.bakko.weather.util.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherRepo {

    private WeatherServiceEndPoint endPoint = ServiceGenerator.createService(WeatherServiceEndPoint.class);

    public void getWeatherByCityId(String cityId, final ResultCallback<WeatherResponse> callback) {
        Call<WeatherResponse> call = endPoint.getCurrentWeatherByCityId(cityId, AppUtils.apiKey);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response.isSuccessful()) {
                    WeatherResponse weatherResponse = response.body();

                    callback.onSuccess(response.body());

                    CurrentWeather weatherInfo = new CurrentWeather();
                    weatherInfo.lastUpdate = new Date(weatherResponse.dt*1000).toString();
                    weatherInfo.description = weatherResponse.weather.get(0).description;
                    weatherInfo.temperature = (weatherResponse.main.temp - 273.15) + '\u00B0' + "C";

                    String wind = "";
                    if (weatherResponse.wind.speed <= 0.3) {
                        wind = "Calm with direction " + weatherResponse.wind.deg + '\u00B0';
                    } else if (weatherResponse.wind.speed <= 1.5) {
                        wind = "Light air with direction " + weatherResponse.wind.deg + '\u00B0';
                    } else if (weatherResponse.wind.speed <= 3.3) {
                        wind = "Light breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    } else if (weatherResponse.wind.speed <= 5.5) {
                        wind = "Gentle breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 7.9) {
                        wind = "Moderate breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 10.7) {
                        wind = "Fresh breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 13.8) {
                        wind = "Strong breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 17.1) {
                        wind = "High wind, moderate gale, near gale with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 20.7) {
                        wind = "Gale, Fresh gale with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 24.4) {
                        wind = "Strong gale with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 28.4) {
                        wind = "Storm, whole gale with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 32.6) {
                        wind = "Violent storm with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else {
                        wind = "Hurricane force with direction " + weatherResponse.wind.deg + '\u00B0';
                    }

                    weatherInfo.windInfo = wind;
                    weatherInfo.pressure = weatherResponse.main.pressure + " mmHg";
                    weatherInfo.humidity = weatherResponse.main.humidity + " %";
                    weatherInfo.sunRise = new SimpleDateFormat("dd MMMM yyyy : HH:mm:ss").format(new Date(weatherResponse.sys.sunrise * 1000));
                    weatherInfo.sunSet = new SimpleDateFormat("HH:mm:ss").format(new Date(weatherResponse.sys.sunset * 1000));
                    weatherInfo.cityName = weatherResponse.cityName;

                } else {
                    callback.isErrors(true, String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                callback.isErrors(true, t.getMessage());
            }
        });
    }

    public void getWeatherByCoordinate(double latitude, final double longitude, final ResultCallback<CurrentWeather> callback) {
        Call<WeatherResponse> call = endPoint.getCurrentWeatherByCoordinate(latitude, longitude, AppUtils.apiKey);

        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response.isSuccessful()) {

                    WeatherResponse weatherResponse = response.body();
                    SimpleDateFormat jdf = new SimpleDateFormat("dd MMM : HH:mm");

                    CurrentWeather weatherInfo = new CurrentWeather();

                    Date date = new Date(weatherResponse.dt * 1000L);
                    weatherInfo.lastUpdate = jdf.format(date);

                    weatherInfo.description = weatherResponse.weather.get(0).description;
                    weatherInfo.temperature = String.format( "%.2f", weatherResponse.main.temp - 273.15) + '\u00B0' + "C";

                    String wind = "";
                    if (weatherResponse.wind.speed <= 0.3) {
                        wind = "Calm with direction " + weatherResponse.wind.deg + '\u00B0';
                    } else if (weatherResponse.wind.speed <= 1.5) {
                        wind = "Light air with direction " + weatherResponse.wind.deg + '\u00B0';
                    } else if (weatherResponse.wind.speed <= 3.3) {
                        wind = "Light breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    } else if (weatherResponse.wind.speed <= 5.5) {
                        wind = "Gentle breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 7.9) {
                        wind = "Moderate breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 10.7) {
                        wind = "Fresh breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 13.8) {
                        wind = "Strong breeze with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 17.1) {
                        wind = "High wind, moderate gale, near gale with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 20.7) {
                        wind = "Gale, Fresh gale with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 24.4) {
                        wind = "Strong gale with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 28.4) {
                        wind = "Storm, whole gale with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else if (weatherResponse.wind.speed <= 32.6) {
                        wind = "Violent storm with direction " + weatherResponse.wind.deg + '\u00B0';
                    }else {
                        wind = "Hurricane force with direction " + weatherResponse.wind.deg + '\u00B0';
                    }

                    weatherInfo.windInfo = wind;
                    weatherInfo.pressure = weatherResponse.main.pressure + " hpa";
                    weatherInfo.humidity = weatherResponse.main.humidity + " %";

                    SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                    Date sunRise = new Date(weatherResponse.sys.sunrise * 1000L);
                    weatherInfo.sunRise = df.format(sunRise);
                    Date sunSet = new Date(weatherResponse.sys.sunset * 1000L);
                    weatherInfo.sunSet = df.format(sunSet);

                    weatherInfo.coordinate = "lat: " + weatherResponse.coord.lat + ", " + "lon: " + weatherResponse.coord.lon;
                    weatherInfo.cityName = weatherResponse.cityName + ", " + weatherResponse.sys.country;

                    callback.onSuccess(weatherInfo);
                } else {
                    callback.isErrors(true, NetworkUtils.errorString(response.code()));
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                callback.isErrors(true, "Network failure, retry.");
            }
        });
    }
}
