package co.bakko.weather.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }
    public static String errorString(int code) {
        String str = "";
        switch (code) {
            case 404:
                str = "Not found";
                break;
            case 500:
                str = "Server broken";
                break;
            default:
                str = "Unknown error";
                break;
        }
        return str;
    }
}
