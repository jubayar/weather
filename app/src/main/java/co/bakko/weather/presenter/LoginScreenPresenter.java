package co.bakko.weather.presenter;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import co.bakko.weather.contact.LoginScreenContact.UserView;
import co.bakko.weather.contact.LoginScreenContact.UserActionListener;
import co.bakko.weather.service.LocationService;
import co.bakko.weather.util.NetworkUtils;
import co.bakko.weather.view.LoginScreenActivity;
import co.bakko.weather.view.LoginScreenFragment;
import co.bakko.weather.view.MainViewActivity;

public class LoginScreenPresenter implements UserActionListener, LocationService.LocationCallback {

    private Context context;
    private LocationService provider;
    private Location location;
    private UserView view;
    private boolean flag = false;

    public LoginScreenPresenter(Context context, UserView userView) {
        this.context = context;
        this.view = userView;
        provider =  new LocationService(context, this);
    }

    @Override
    public void connectLocationService() {
        provider.connectGPS();
        provider.connect();
    }

    @Override
    public void facebookLogin(final LoginScreenFragment fragment, CallbackManager callbackManager) {

        if (!NetworkUtils.isInternetAvailable(context)) {
            view.showNoInternetLayout(true);
            return;
        }

        if (!provider.checkGPSStatus()) {
            view.showMsg("Enable GPS");
            return;
        }

        view.enableButton(false);

        List<String> permissionNeeds = Arrays.asList("email","public_profile");
        LoginManager.getInstance().logInWithReadPermissions(fragment, permissionNeeds);

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResults) {
                fetchUserDataFromFacebook(loginResults.getAccessToken());
                view.enableButton(false);
                view.showProgressIndicator(true);
            }

            @Override
            public void onCancel() {
                view.showMsg("Login is Canceled, try again.");
                view.enableButton(true);
            }

            @Override
            public void onError(FacebookException error) {
                view.showMsg("Facebook login error, try again.");
                view.enableButton(true);
            }
        });
    }

    @Override
    public void forwardToMainView(Location location) {
        view.showProgressIndicator(false);
        MainViewActivity.start(context, location.getLatitude(), location.getLongitude());
        flag = false;
    }

    @Override
    public void onDestroy() {
        provider.disconnect();
        context = null;
        provider = null;
        location = null;
    }

    @Override
    public void handleNewLocation(Location location) {
        if (location != null) {
            this.location = location;

            if (flag)
                forwardToMainView(location);
        }
    }

    public void fetchUserDataFromFacebook(AccessToken accessToken) {

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                try {
                    view.showMsg(object.getString("name") + ", Welcome to weather Application");
                    if (location !=  null) {
                        forwardToMainView(location);
                        flag = false;
                    } else {
                        flag = true;
                        view.showMsg("Searching location, Please wait.");
                    }

                } catch (Exception e) {
                    view.showMsg("Facebook server error");
                    view.enableButton(true);
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }
}
