package co.bakko.weather.presenter;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import co.bakko.weather.contact.HourlyForecastContact.UserActionListener;
import co.bakko.weather.contact.HourlyForecastContact.UserView;
import co.bakko.weather.contact.callback.ResultCallback;
import co.bakko.weather.model.ForecastInfo;
import co.bakko.weather.model.ForecastResponse;
import co.bakko.weather.model.ForecastWeather;
import co.bakko.weather.repo.HourlyForecastRepo;
import co.bakko.weather.util.NetworkUtils;

public class HourlyForecastPresenter implements UserActionListener {

    private final UserView view;
    private HourlyForecastRepo repo;
    private double latitude;
    private double longitude;

    public HourlyForecastPresenter(UserView view, double latitude, double longitude) {
        this.view = view;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void onStart(Context context) {
        repo = new HourlyForecastRepo();
        view.setSwipeRefreshLayout(true);
        checkSetting(context);
    }

    @Override
    public void checkSetting(Context context) {
        if (!NetworkUtils.isInternetAvailable(context)) {
            view.showNoInternetLayout(true);
            view.setSwipeRefreshLayout(false);
        } else {
            view.showNoInternetLayout(false);
            view.setSwipeRefreshLayout(true);
            fetchWeatherForecast();
        }
    }

    @Override
    public void fetchWeatherForecast() {

        repo.get5DaysForecastByCoordinate(latitude, longitude, new ResultCallback<ForecastResponse>() {
            @Override
            public void isErrors(boolean isError, String msg) {
                if (isError) {
                    view.showErrorMsg(msg);
                    view.setSwipeRefreshLayout(false);
                }
            }

            @Override
            public void onSuccess(ForecastResponse result) {
                List<ForecastInfo> list = new ArrayList<ForecastInfo>();

                for (ForecastWeather weather: result.list) {
                    ForecastInfo info = new ForecastInfo();
                    info.forecastTime = weather.dt_txt;
                    info.temperature = String.format( "%.2f", (weather.main.temp - 273.15) ) + '\u00B0' + "C";
                    info.description = weather.weather.get(0).description;

                    String wind = "";
                    if (weather.wind.speed <= 0.3) {
                        wind = "Calm with direction " + weather.wind.deg + '\u00B0';
                    } else if (weather.wind.speed <= 1.5) {
                        wind = "Light air with direction " + weather.wind.deg + '\u00B0';
                    } else if (weather.wind.speed <= 3.3) {
                        wind = "Light breeze with direction " + weather.wind.deg + '\u00B0';
                    } else if (weather.wind.speed <= 5.5) {
                        wind = "Gentle breeze with direction " + weather.wind.deg + '\u00B0';
                    }else if (weather.wind.speed <= 7.9) {
                        wind = "Moderate breeze with direction " + weather.wind.deg + '\u00B0';
                    }else if (weather.wind.speed <= 10.7) {
                        wind = "Fresh breeze with direction " + weather.wind.deg + '\u00B0';
                    }else if (weather.wind.speed <= 13.8) {
                        wind = "Strong breeze with direction " + weather.wind.deg + '\u00B0';
                    }else if (weather.wind.speed <= 17.1) {
                        wind = "High wind, moderate gale, near gale with direction " + weather.wind.deg + '\u00B0';
                    }else if (weather.wind.speed <= 20.7) {
                        wind = "Gale, Fresh gale with direction " + weather.wind.deg + '\u00B0';
                    }else if (weather.wind.speed <= 24.4) {
                        wind = "Strong gale with direction " + weather.wind.deg + '\u00B0';
                    }else if (weather.wind.speed <= 28.4) {
                        wind = "Storm, whole gale with direction " + weather.wind.deg + '\u00B0';
                    }else if (weather.wind.speed <= 32.6) {
                        wind = "Violent storm with direction " + weather.wind.deg + '\u00B0';
                    }else {
                        wind = "Hurricane force with direction " + weather.wind.deg + '\u00B0';
                    }

                    info.windInfo = wind;
                    info.humidity = weather.main.humidity + " %";

                    list.add(info);
                }

                view.setSwipeRefreshLayout(false);
                view.showWeatherInfo(list);
            }
        });
    }

    @Override
    public void onDestroy() {
        repo = null;
    }
}
