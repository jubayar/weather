package co.bakko.weather.presenter;

import android.content.Context;

import co.bakko.weather.contact.CurrentWeatherContact.UserActionListener;
import co.bakko.weather.contact.CurrentWeatherContact.UserView;
import co.bakko.weather.contact.callback.ResultCallback;
import co.bakko.weather.model.CurrentWeather;
import co.bakko.weather.repo.WeatherRepo;
import co.bakko.weather.util.NetworkUtils;

public class CurrentWeatherPresenter implements UserActionListener {

    private final UserView view;
    private WeatherRepo repo;
    private double latitude;
    private double longitude;

    public CurrentWeatherPresenter(UserView userView, double latitude, double longitude) {
        this.view = userView;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void onStart(Context context) {
        repo = new WeatherRepo();
        view.setSwipeRefreshLayout(true);
        checkSetting(context);
    }

    @Override
    public void checkSetting(Context context) {
        if (!NetworkUtils.isInternetAvailable(context)) {
            view.showNoInternetLayout(true);
            view.setSwipeRefreshLayout(false);
        } else {
            view.showNoInternetLayout(false);
            view.setSwipeRefreshLayout(true);
            fetchWeatherInfo();
        }
    }

    @Override
    public void fetchWeatherInfo() {

        repo.getWeatherByCoordinate(latitude, longitude, new ResultCallback<CurrentWeather>() {
            @Override
            public void isErrors(boolean isError, String msg) {
                if (isError) {
                    view.showErrorMsg(msg);
                    view.setSwipeRefreshLayout(false);
                }
            }

            @Override
            public void onSuccess(CurrentWeather result) {
                view.setSwipeRefreshLayout(false);
                view.showWeatherInfo(result);
            }
        });
    }

    @Override
    public void onDestroy() {
        repo = null;
    }
}
