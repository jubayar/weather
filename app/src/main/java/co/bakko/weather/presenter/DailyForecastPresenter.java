package co.bakko.weather.presenter;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.bakko.weather.contact.DailyForecastContact.UserActionListener;
import co.bakko.weather.contact.DailyForecastContact.UserView;
import co.bakko.weather.contact.callback.ResultCallback;
import co.bakko.weather.model.DailyForecast;
import co.bakko.weather.model.DailyForecastResponse;
import co.bakko.weather.model.DailyInfo;
import co.bakko.weather.repo.DailyForecastRepo;
import co.bakko.weather.util.NetworkUtils;


public class DailyForecastPresenter implements UserActionListener {

    private final UserView view;
    private DailyForecastRepo repo;
    private double latitude;
    private double longitude;

    public DailyForecastPresenter(UserView view, double latitude, double longitude) {
        this.view = view;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void onStart(Context context) {
        repo = new DailyForecastRepo();
        view.setSwipeRefreshLayout(true);
        checkSetting(context);
    }

    @Override
    public void checkSetting(Context context) {
        if (!NetworkUtils.isInternetAvailable(context)) {
            view.showNoInternetLayout(true);
            view.setSwipeRefreshLayout(false);
        } else {
            view.showNoInternetLayout(false);
            view.setSwipeRefreshLayout(true);
            fetchWeatherForecast();
        }
    }

    @Override
    public void fetchWeatherForecast() {

        repo.getDailyForecastByCoordinate(latitude, longitude, 5, new ResultCallback<DailyForecastResponse>() {
            @Override
            public void isErrors(boolean isError, String msg) {
                if (isError) {
                    view.showErrorMsg(msg);
                    view.setSwipeRefreshLayout(false);
                }
            }

            @Override
            public void onSuccess(DailyForecastResponse result) {
                List<DailyInfo> dailyInfos = new ArrayList<DailyInfo>();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM");

                for (DailyForecast dailyForecast : result.list) {

                    DailyInfo dailyInfo = new DailyInfo();

                    Date date = new Date(dailyForecast.dt * 1000L);
                    dailyInfo.forecastTime = dateFormat.format(date);

                    dailyInfo.temp = String.format( "%.2f", dailyForecast.temp.day - 273.15) + '\u00B0' + "C";
                    dailyInfo.minTemp = String.format( "%.2f", dailyForecast.temp.min - 273.15) + '\u00B0' + "C";
                    dailyInfo.maxTemp = String.format( "%.2f", dailyForecast.temp.max - 273.15) + '\u00B0' + "C";
                    dailyInfo.clouds = dailyForecast.clouds + " %";
                    dailyInfo.description = dailyForecast.weather.get(0).description;
                    dailyInfo.humidity = dailyForecast.humidity + " %";

                    String wind = "";
                    if (dailyForecast.speed <= 0.3) {
                        wind = "Calm with direction ";
                    } else if (dailyForecast.speed <= 1.5) {
                        wind = "Light air with direction ";
                    } else if (dailyForecast.speed <= 3.3) {
                        wind = "Light breeze with direction ";
                    } else if (dailyForecast.speed <= 5.5) {
                        wind = "Gentle breeze with direction ";
                    }else if (dailyForecast.speed <= 7.9) {
                        wind = "dailyForecast breeze with direction ";
                    }else if (dailyForecast.speed <= 10.7) {
                        wind = "Fresh breeze with direction ";
                    }else if (dailyForecast.speed <= 13.8) {
                        wind = "Strong breeze with direction ";
                    }else if (dailyForecast.speed <= 17.1) {
                        wind = "High wind, moderate gale, near gale with direction ";
                    }else if (dailyForecast.speed <= 20.7) {
                        wind = "Gale, Fresh gale with direction ";
                    }else if (dailyForecast.speed <= 24.4) {
                        wind = "Strong gale with direction ";
                    }else if (dailyForecast.speed <= 28.4) {
                        wind = "Storm, whole gale with direction ";
                    }else if (dailyForecast.speed <= 32.6) {
                        wind = "Violent storm with direction ";
                    }else {
                        wind = "Hurricane force with direction ";
                    }

                    dailyInfo.windInfo = wind + dailyForecast.deg + '\u00B0';

                    dailyInfos.add(dailyInfo);
                }

                view.setSwipeRefreshLayout(false);
                view.showWeatherInfo(dailyInfos);

            }
        });
    }

    @Override
    public void onDestroy() {
        repo = null;
    }
}
